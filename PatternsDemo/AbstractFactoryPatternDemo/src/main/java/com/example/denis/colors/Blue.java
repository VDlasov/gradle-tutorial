package com.example.denis.colors;

/**
 * Created by Denis on 06.09.2015.
 */
public class Blue implements Color {

    @Override
    public void fill() {
        System.out.println("Inside Blue::fill() method.");
    }
}
