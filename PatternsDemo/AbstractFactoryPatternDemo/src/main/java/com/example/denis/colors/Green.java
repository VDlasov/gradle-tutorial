package com.example.denis.colors;

/**
 * Created by Denis on 06.09.2015.
 */
public class Green implements Color {

    @Override
    public void fill() {
        System.out.println("Inside Green::fill() method.");
    }
}
